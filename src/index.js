import "./index.css";
import "./app.css";

const getData = () => {
  const el = document.getElementById("gumroad-data");
  if (!el) return {};

  try {
    return JSON.parse(el.innerText);
  } catch (e) {
    return {};
  }
};

Array.from(document.getElementsByTagName("a")).forEach((link) => {
  const { host } = getData();
  if (!link.href) return;
  console.log(link.href);
  if (!link.href.match(host)) return;

  link.onclick = (event) => {
    event.preventDefault();

    const container = document.createElement("div");

    container.innerHTML = `
      <div class="app-modal">
        <iframe class="frame" src="${link.href}" width="80%" height="80%" />
      </div>
    `;
    const modal = container.children[0];

    document.body.appendChild(modal);

    window.addEventListener("message", (event) => {
      try {
        const data = JSON.parse(event.data);
        if (data.action === "close") {
          modal.remove();
        }
      } catch (e) {}
    });
  };
});
